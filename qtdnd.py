import sys
from PyQt5 import QtWidgets
import diceui
import random
import characterEditui
from functools import partial
import shelve

def rollDice(window, amount, sides, mod):
    result=0
    result2=0
    for i in range(amount):
        x = random.randrange(1,sides)
        print(x)
        result += x
        #print("result: " + str(result))
    #print(result)
    
    if window.ui.advYes.isChecked():
        for i in range(amount):
            x = random.randrange(1,sides)
            print(x)
            result2 += x
        if result2 > result:
            result = result2
    elif window.ui.advNeg.isChecked():
        for i in range(amount):
            x = random.randrange(1,sides)
            print(x)
            result2 += x
        if result2 < result:
            result = result2
            
    window.ui.rollResult.setText(str(int(result + mod)))
    

class character():
    def __init__(self, givenName):
        self.name = givenName
        self.stats = {
        'Strength': 0,
        'Dexterity' : 0,
        'Constitution': 0,
        'Intelligence': 0,
        'Wisdom': 0,
        'Charisma': 0
        }
        self.proficiencies = {
        'Acrobatics' : False,
        'Animal Handling' : False,
        'Arcana' : False,
        'Athletics' : False,
        'Deception': False,
        'History' : False,
        'Insight' : False,
        'Intimidation' : False,
        'Investigation' : False,
        'Medicine' : False,
        'Nature' : False,
        'Perception' : False,
        'Preformance' : False,
        'Persuasion' : False,
        'Religion' : False,
        'Sleight of Hand' : False,
        'Stealth' : False,
        'Survival' : False
        }
        self.bonus = 2
        

class characterEdit(QtWidgets.QWidget):
    def __init__(self):
        super(characterEdit, self).__init__()
        self.ui = characterEditui.Ui_CharacterEdit()
        self.ui.setupUi(self)
        
        for i in characters:
            self.ui.charList.addItem(i)
        
        self.ui.charList.clicked.connect(self.pull)
        
        self.ui.newCharacter.clicked.connect(self.makeNew)
        
        self.ui.applyButton.clicked.connect(self.save)
        
        self.ui.refresh.clicked.connect(self.refresh)
        
        self.ui.deleteButton.clicked.connect(self.delete)
        
        self.ui.deleteButton.clicked.connect(self.refresh)
        
    def refresh(self):
        self.ui.charList.clear()
        for i in characters:
            self.ui.charList.addItem(i)
        
    def save(self):
        self.char.stats['Strength'] = self.ui.strEdit.value()
        self.char.stats['Dexterity'] = self.ui.dexEdit.value()
        self.char.stats['Constitution'] = self.ui.constEdit.value()
        self.char.stats['Intelligence'] = self.ui.intEdit.value()
        self.char.stats['Wisdom'] = self.ui.wisEdit.value()
        self.char.stats['Charisma'] = self.ui.charEdit.value()
        
        self.char.proficiencies['Acrobatics'] = self.ui.acrobaticsCheck.isChecked()
        self.char.proficiencies['Animal Handling'] = self.ui.animalCheck.isChecked()
        self.char.proficiencies['Arcana'] = self.ui.arcanaCheck.isChecked()
        self.char.proficiencies['Athletics'] = self.ui.athleticsCheck.isChecked()
        self.char.proficiencies['Deception'] = self.ui.decepionCheck.isChecked()
        self.char.proficiencies['History'] = self.ui.historyCheck.isChecked()
        self.char.proficiencies['Insight'] = self.ui.insightCheck.isChecked()
        self.char.proficiencies['Intimidation'] = self.ui.intimidationCheck.isChecked()
        self.char.proficiencies['Investigation'] = self.ui.investigationCheck.isChecked()
        self.char.proficiencies['Medicine'] = self.ui.medicine.isChecked()
        self.char.proficiencies['Nature'] = self.ui.natureCheck.isChecked()
        self.char.proficiencies['Perception'] = self.ui.perceptionCheck.isChecked()
        self.char.proficiencies['Preformance'] = self.ui.preformanceCheck.isChecked()
        self.char.proficiencies['Persuasion'] = self.ui.persuasionCheck.isChecked()
        self.char.proficiencies['Religion'] = self.ui.religionCheck.isChecked()
        self.char.proficiencies['Sleight of Hand'] = self.ui.sleightCheck.isChecked()
        self.char.proficiencies['Stealth'] = self.ui.stealthCheck.isChecked()
        self.char.proficiencies['Survival'] = self.ui.survivalCheck.isChecked()
        self.char.bonus = self.ui.profBonus.value()
        
        with shelve.open('characters.db') as db:
            db['characters'] = characters
            db.close()    

        
        
            
    def pull(self):
        sel = self.ui.charList.currentItem()
        #char = self.ui.charList.selectedItems()
        for i in characters:
            if i == sel.text():
                self.char = characters[i]
                break
        self.ui.nameEdit.setText(str(self.char.name))
        self.ui.strEdit.setValue(self.char.stats["Strength"])
        self.ui.dexEdit.setValue(self.char.stats["Dexterity"])
        self.ui.constEdit.setValue(self.char.stats["Constitution"])
        self.ui.intEdit.setValue(self.char.stats["Intelligence"])
        self.ui.wisEdit.setValue(self.char.stats["Wisdom"])
        self.ui.charEdit.setValue(self.char.stats["Charisma"])
        
        self.ui.acrobaticsCheck.setChecked(self.char.proficiencies['Acrobatics'])
        self.ui.animalCheck.setChecked(self.char.proficiencies['Animal Handling'])
        self.ui.arcanaCheck.setChecked(self.char.proficiencies['Arcana'])
        self.ui.athleticsCheck.setChecked(self.char.proficiencies['Athletics'])
        self.ui.decepionCheck.setChecked(self.char.proficiencies['Deception'])
        self.ui.historyCheck.setChecked(self.char.proficiencies['History'])
        self.ui.insightCheck.setChecked(self.char.proficiencies['Insight'])
        self.ui.intimidationCheck.setChecked(self.char.proficiencies['Intimidation'])
        self.ui.investigationCheck.setChecked(self.char.proficiencies['Investigation'])
        self.ui.medicine.setChecked(self.char.proficiencies['Medicine'])
        self.ui.natureCheck.setChecked(self.char.proficiencies['Nature'])
        self.ui.perceptionCheck.setChecked(self.char.proficiencies['Perception'])
        self.ui.preformanceCheck.setChecked(self.char.proficiencies['Preformance'])
        self.ui.persuasionCheck.setChecked(self.char.proficiencies['Persuasion'])
        self.ui.religionCheck.setChecked(self.char.proficiencies['Religion'])
        self.ui.sleightCheck.setChecked(self.char.proficiencies['Sleight of Hand'])
        self.ui.stealthCheck.setChecked(self.char.proficiencies['Stealth'])
        self.ui.survivalCheck.setChecked(self.char.proficiencies['Survival'])
        self.ui.profBonus.setValue(self.char.bonus)
        
    
    def makeNew(self):

        i, ok = QtWidgets.QInputDialog.getText(self, "Get Name", "Name:")
        if ok:
            characters[i] = character(i)
            self.ui.charList.addItem(i)
    
    def delete(self):
        delList = []
        for i in characters:
            if characters[i] == self.char:
                delList.append(i)
        for i in delList:
            del characters[i]
        with shelve.open('characters.db') as db:
            db['characters'] = characters
            db.close() 
        
class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = diceui.Ui_mainwindow()
        self.my_characterEdit = characterEdit()
        self.ui.setupUi(self)
        
        for i in characters:
            self.ui.charList.addItem(i)
            self.ui.charList.setCurrentRow(0)
            self.pull()
        
        self.ui.roll.clicked.connect(self.rollDice)
        
        self.ui.editChar.clicked.connect(self.my_characterEdit.show)
        
        self.ui.charList.clicked.connect(self.pull)
        
        self.ui.charList.clicked.connect(self.refresh)
        
        self.ui.str.clicked.connect(self.rollStr)
        
        self.ui.intelligence.clicked.connect(self.rollInt)
        
        self.ui.dexterity.clicked.connect(self.rollDex)
        
        self.ui.charisma.clicked.connect(self.rollChar)
        
        self.ui.constitution.clicked.connect(self.rollConst)
        
        self.ui.wisdom.clicked.connect(self.rollWis)
        
        self.ui.acrobatics.clicked.connect(self.rollAcrobatics)
        
        self.ui.athletics.clicked.connect(self.rollAthletics)
        
        self.ui.sleight.clicked.connect(self.rollSleight)
        
        self.ui.stealth.clicked.connect(self.rollStealth)
        
        self.ui.arcana.clicked.connect(self.rollArcana)
        
        self.ui.history.clicked.connect(self.rollHistory)
        
        self.ui.investigation.clicked.connect(self.rollInvestigation)
        
        self.ui.nature.clicked.connect(self.rollNature)
        
        self.ui.religion.clicked.connect(self.rollReligion)
        
        self.ui.animal.clicked.connect(self.rollAnimal)
        
        self.ui.insight.clicked.connect(self.rollInsight)
        
        self.ui.medicine.clicked.connect(self.rollMedicine)
        
        self.ui.perception.clicked.connect(self.rollPerception)
        
        self.ui.survival.clicked.connect(self.rollSurvival)
        
        self.ui.deception.clicked.connect(self.rollDeception)
        
        self.ui.intimidation.clicked.connect(self.rollIntimidation)
        
        self.ui.preformance.clicked.connect(self.rollPreformance)
        
        self.ui.persuasion.clicked.connect(self.rollPersuasion)
        

    def rollDice(self):
        amount = self.ui.amount.value()
        sides = self.ui.sides.value()
        mod = self.ui.mod.value()
        rollDice(self, amount, sides, mod)

    def pull(self):
        sel = self.ui.charList.currentItem()
        #char = self.ui.charList.selectedItems()
        for i in characters:
            if i == sel.text():
                self.char = characters[i]
                break
        self.ui.characterName.setText(str(self.char.name))
    
    def refresh(self):
        self.ui.charList.clear()
        for i in characters:
            self.ui.charList.addItem(i)
    
    def rollStr(self):
        rollDice(self, 1, 20, ((self.char.stats["Strength"]-10)/2))
    
    def rollInt(self):
        rollDice(self, 1, 20, ((self.char.stats["Intelligence"]-10)/2))
    
    def rollDex(self):
        rollDice(self, 1, 20, ((self.char.stats["Dexterity"]-10)/2))
        
    def rollConst(self):
        rollDice(self, 1, 20, ((self.char.stats["Constitution"]-10)/2))
        
    def rollChar(self):
        rollDice(self, 1, 20, ((self.char.stats["Charisma"]-10)/2))
        
    def rollWis(self):
        rollDice(self, 1, 20, ((self.char.stats["Wisdom"]-10)/2))
        
    def rollAthletics(self):
        rollDice(self, 1, 20, ((self.char.stats["Strength"]-10)/2)+int(self.char.proficiencies["Athletics"]))
        print(int(self.char.proficiencies["Athletics"]))

    def rollAcrobatics(self):
        rollDice(self, 1, 20, ((self.char.stats["Dexterity"]-10)/2)+int(self.char.proficiencies["Acrobatics"])*self.char.bonus)

    def rollSleight(self):
        rollDice(self, 1, 20, ((self.char.stats["Dexterity"]-10)/2)+int(self.char.proficiencies["Sleight of Hand"])*self.char.bonus)
        
    def rollStealth(self):
        rollDice(self, 1, 20, ((self.char.stats["Dexterity"]-10)/2)+int(self.char.proficiencies["Stealth"])*self.char.bonus)

    def rollArcana(self):
        rollDice(self, 1, 20, ((self.char.stats["Intelligence"]-10)/2)+int(self.char.proficiencies["Arcana"])*self.char.bonus)

    def rollHistory(self):
        rollDice(self, 1, 20, ((self.char.stats["History"]-10)/2)+int(self.char.proficiencies["History"])*self.char.bonus)
    
    def rollInvestigation(self):
        rollDice(self, 1, 20, ((self.char.stats["Intelligence"]-10)/2)+int(self.char.proficiencies["Investigation"])*self.char.bonus)
        
    def rollNature(self):
        rollDice(self, 1, 20, ((self.char.stats["Intelligence"]-10)/2)+int(self.char.proficiencies["Nature"])*self.char.bonus)

    def rollReligion(self):
        rollDice(self, 1, 20, ((self.char.stats["Intelligence"]-10)/2)+int(self.char.proficiencies["Religion"])*self.char.bonus)
        
    def rollAnimal(self):
        rollDice(self, 1, 20, ((self.char.stats["Wisdom"]-10)/2)+int(self.char.proficiencies["Animal Handling"])*self.char.bonus)

    def rollInsight(self):
        rollDice(self, 1, 20, ((self.char.stats["Wisdom"]-10)/2)+int(self.char.proficiencies["Insight"])*self.char.bonus)
        
    def rollMedicine(self):
        rollDice(self, 1, 20, ((self.char.stats["Wisdom"]-10)/2)+int(self.char.proficiencies["Medicine"])*self.char.bonus)

    def rollPerception(self):
        rollDice(self, 1, 20, ((self.char.stats["Wisdom"]-10)/2)+int(self.char.proficiencies["Perception"])*self.char.bonus)
        
    def rollSurvival(self):
        rollDice(self, 1, 20, ((self.char.stats["Wisdom"]-10)/2)+int(self.char.proficiencies["Survival"])*self.char.bonus)
        
    def rollIntimidation(self):
        rollDice(self, 1, 20, ((self.char.stats["Charisma"]-10)/2)+int(self.char.proficiencies["Intimidation"])*self.char.bonus)

    def rollDeception(self):
        rollDice(self, 1, 20, ((self.char.stats["Charisma"]-10)/2)+int(self.char.proficiencies["Deception"])*self.char.bonus)
        
    def rollPreformance(self):
        rollDice(self, 1, 20, ((self.char.stats["Charisma"]-10)/2)+int(self.char.proficiencies["Preformance"])*self.char.bonus)
        
    def rollPersuasion(self):
        rollDice(self, 1, 20, ((self.char.stats["Charisma"]-10)/2)+int(self.char.proficiencies["Persuasion"])*self.char.bonus)        
            
try:
    with shelve.open('characters.db') as db:
        characters = db['characters']
        db.close()
except:
    shelve.open('characters.db')
    with shelve.open('characters.db') as db:
        db['characters'] = {'Chana' : character('Chana')}
        characters = db['characters']
        db.close()
    


app = QtWidgets.QApplication(sys.argv)
my_mainWindow = MainWindow()
my_mainWindow.show()

sys.exit(app.exec_())
